# ICIP 2023


# SUBJECTIVE ASSESSMENT OF THE IMPACT OF A CONTENT ADAPTIVE OPTIMISER FOR COMPRESSING 4K HDR CONTENT WITH AV1

Author: Vibhoothi, Angeliki Katsenou, Francois Pitie, Katarina Domijan, Anil
Kokaram

Contact: {vibhoothi, anil.kokaram} at TCD dot IE

## Table of Contents 
[[_TOC_]]


## Abstract
Since 2015 video dimensionality has expanded to higher spatial and
temporal resolutions and a wider colour gamut. This High Dynamic
Range (HDR) content has gained traction in the consumer space as
it delivers an enhanced quality of experience. At the same time,
the complexity of codecs is growing. This has driven the development of tools
for content-adaptive optimisation that achieve optimal rate-distortion
performance for HDR video at 4K resolution. While
improvements of just a few percentage points in BDRATE (1-5%)
are significant for the streaming media industry, the impact on subjective
quality has been less studied especially for HDR/AV1. In this paper, we conduct
a subjective quality assessment (42 subjects) of
4K HDR content with a per-clip optimisation strategy. We correlate
these subjective scores with existing popular objective metrics used in standard
development and show that some perceptual metrics correlate surprisingly well
even though they are not tuned for HDR. We find that the DSQCS protocol is too
insensitive to categorically compare the methods but the data allows us to make
recommendations
about the use of experts vs non-experts in HDR studies, and explain
the subjective impact of film grain in HDR content under compression.


## Dataset

| **#** | **Name**                                                   | **FPS** | **Frames** |
|-------|------------------------------------------------------------|---------|------------|
| 1     | aomcosmos_3840x2160_24_hdr10_11589-11752.y4m               | 24      | 130        |
| 2     | wipe_3840x2160_5994_10bit_420_8750-9050.y4m                | 59.94   | 300        |
| 3     | NocturneRoom_3840x2160_60fps_hdr10.y4m                     | 60      | 300        |
| 4     | svtsmithy_3840x2160_50_10bit_420p.y4m                      | 50      | 250        |
| 5     | aomsol_levante_3840x2160_24_hdr10_2268-2412.y4m            | 24      | 130        |
| 6     | aommeridian1_3840x2160_5994_hdr10_15932-16309.y4m          | 59.94   | 300        |
| 7     | aommeridian2_3840x2160_5994_hdr10_22412-22738_train_R0.y4m | 59.94   | 300        |



The source for these clips are:
1) [Netflix Open Content](https://opencontent.netflix.com/), also available at
[AOM-CTC](https://media.xiph.org/video/aomctc/test_set/hdr1_4k/),
[AOM-Candidates](https://media.xiph.org/video/av2/), [Netflix](http://download.opencontent.netflix.com.s3.amazonaws.com/index.html?prefix=aom_test_materials/HDR/).
1) [Cables 4K](https://www.cablelabs.com/4k)
1) [SVT Open Content](https://www.svt.se/open/en/content/)



## Encoder Configuration
libaom-av1-3.2.0, [287164d](https://aomedia.googlesource.com/aom/+/287164d)

Random-Access Preset as per [AOM-CTC](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)
``` sh
$AOMEMC --cpu-used=0 --passes=1 --lag-in-frames=19 --auto-alt-ref=1 --min-gf-interval=16 --max-gf-interval=16
--gf-min-pyr-height=4 --gf-max-pyr-height=4 --limit=130 --kf-min-dist=65 --kf-max-dist=65 --use-fixed-qp-offsets=1
--deltaq-mode=0 --enable-tpl-model=0 --end-usage=q --cq-level=$Q --enable-keyframe-filtering=0 --threads=1
--test-decode=fatal -o output.ivf --color-primaries=bt2020 --transfer-characteristics=smpte2084
--matrix-coefficients=bt2020ncl --chroma-sample-position=colocated $OPTIONS $INPUT.Y4M
```
> GOP-Size is fixed at 65, Sub-GOP is 16, Temporal-layers is 4, Closed-GOP
GOP-Size is set in AV1 with help of `kf-min-dist` and `kf-max-dist`. Sub-GOP
size is set in AV1 using `min-gf-interval` and `max-gf-interval`. The temporal
layers is defined using `gf-min-pyr-height` and `gf-max-pyr-height`. To have a
hierarchical reference structure, the number of temporal layers should be set to
$log2(SubGopsize)$. Open-GOP and Closed GOP is controlled with help of `
--enable-fwd-kf` inside AV1. Color Primaries, Transfer Charecteristics, Matrix
Coffecients are also specified which is required HDR Metadata.


## Quantization Parameters(QP)

| SL-No | libaom-av1 |
| ----- | ---------- |
| 1     | 27         |
| 2     | 39         |
| 3     | 49         |
| 4     | 59         |
| 5     | 63         |

The QP points were chosen based on the [AOM-Common Testing Configuration(CTC.)](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf).


## *Encoder Modifications*
In, libaom-AV1, We have modifed the release version to support feeding
$`\lambda`$ values via command-line argument.


## *Optimiser Setup*
For this experiment, we using Powell method as adopted by [1.], which is implemented in Scipy's optimise module
[scipy.optimise.minimise](https://docs.scipy.org/doc/scipy/reference/optimize.minimize-powell.html#optimize-minimize-powell).


## Data Collection

The RD_points are computed using
[libvmaf v0.2.1](https://github.com/Netflix/vmaf/tree/v2.2.1/libvmaf). For HDR
metrics, we are using HDRTools [v0.22 release branch](https://gitlab.com/standards/HDRTools/-/tree/0.22-dev)


| SL No | Metrics Name | SL No    | Metrics Name  |
| ----- | ------------ | -------- | ------------- |
| 1     | PSNR Y       | 9        | PSNR-HVS Cb   |
| 2     | PSNR Cb      | 10       | PSNR-HVS Cr   |
| 3     | PSNR Cr      | 11       | Encoding Time |
| 4     | PSNR-HVS     | 12       | Decoding Time |
| 5     | CIEDE2000    | 13       | VMAF          |
| 6     | SSIM         | 14       | VMAF-NEG      |
| 7     | MS-SSIM      | 15       | APSNR Y       |
| 8     | PSNR-HVS Y   | 16       | APSNR Cb      |
| 17    | APSNR Cr     | 18       | DE100         |
| 19    | PSNRL100     | 20       | wPSNR-Y       |
| 21    | wPSNR-U      | 22       | wPSNR-V       |
| 23    | HDR-VQM      | 24       | HDR-VDP-3     |

The configuration files used to compute the metrics are adopted from the [3GPP
study](https://github.com/haudiobe/5GVideo/). This is available in the
[cfg](src/cfg/) folder of this material.

For ease of computation, we have also provided 3 scripts for computing them,
- HDR-VQM: `./HDR_VQM.sh $SRC-%05d.exr W H FPS NF $DST-%05d.exr`
- DeltaE+L100: `./HDRMetrics.sh $SRC-%05d.exr W H FPS NF $DST-%05d.exr`
- wPSNR: `./HDRMetrics_psnr.sh $SRC.yuv W H FPS NF $DST.yuv`

Sample command: `./HDR_VQM.sh
aom_cosmos_3840x2160_24_hdr10_1573-1749.y4m-src-%05d.exr 3840 2160 24 130
aom_cosmos_3840x2160_24_hdr10_1573-1749.y4m-compressed-%05d.exr`

For computing HDR-VDP-3, we used "[Code for Video Quality Metrics for
HDR](https://github.com/JoshuaEbenezer/hdr_fr_code/)". The usage is via Matlab
interface, and the source-code for simulations is 