clear
addpath(genpath('./hdrvdp-3.0.6'))
video_dir = '/media/icip2023-subjective/yuv/';
dist_dir = '/media/icip2023-subjective-dist/yuv/';
out_dir = 'hdrvdp3/';
T = readtable('clip_lut.csv');
rng(0,'twister');
%parpool(15)
all_names = [];
all_scores = [];
all_scores_p_det = [];
all_scores_c_max = [];
all_scores_q_jod = [];
ref_yuv_names = T.bvi_name;
framenums = T.fno;
dist_table =readtable('src_vid_dist.csv','Delimiter', ',');
% Training videos
dist_table = dist_table(end-7:end,:);
disp(dist_table);
dis_names = dist_table.dist_vid;

width = 3840;
height = 2160;

pixels_per_degree =  hdrvdp_pix_per_deg( 29.5, [3840 2160], 1.26 );

for dis_index = 1:numel(dis_names)
    dis_score = [];
    p_det_score = [];
    c_max_score = [];
    q_jod_score = [];

    % Get the distored video name, source video name
    dis_name = char(dis_names(dis_index));
    yuv_name = char(dist_table(contains(dist_table.dist_vid, dis_name),:).src_vid);
    ref_upscaled_name = append(yuv_name,'.yuv');
    full_ref_yuv_name = fullfile(video_dir,ref_upscaled_name);

    dis_upscaled_name = dis_name;
    full_yuv_name = fullfile(dist_dir,dis_upscaled_name);

    r =  T(contains(T.bvi_name,yuv_name),:).fno;

    for framenum_index=1:r
        framenum = framenum_index;
        print_line = sprintf("%d, %s", framenum, dis_name);
        disp(print_line);
        [refY,refU,refV,status_ref] = yuv_import(full_ref_yuv_name,[width,height],framenum,'YUV420_16');
        ref_YUV = cast(cat(3,refY,refU,refV),'uint16');
        ref_rgb_bt2020 = ycbcr2rgbwide(ref_YUV,10);
        ref_rgb_bt2020_linear = eotf_pq(ref_rgb_bt2020);

        full_yuv_name = fullfile(dist_dir,dis_upscaled_name);
        [disY,disU,disV,status_dis] = yuv_import(full_yuv_name,[width,height],framenum,'YUV420_16');
        if(status_dis==0)
            disp(strcat("Error reading frame in ",full_yuv_name));
        end

        dis_YUV = cast(cat(3,disY,disU,disV),'uint16');
        dis_rgb_bt2020 = ycbcr2rgbwide(dis_YUV,10);
        dis_rgb_bt2020_linear = eotf_pq(dis_rgb_bt2020);

        %vdp_result = 0;
        vdp_result = hdrvdp3('quality',dis_rgb_bt2020_linear,ref_rgb_bt2020_linear,...
            'rgb-native',pixels_per_degree,{'rgb_display','oled'});

        dis_score   = [dis_score vdp_result.Q];
        p_det_score = [p_det_score vdp_result.P_det];
        c_max_score = [c_max_score vdp_result.C_max];
        q_jod_score = [q_jod_score vdp_result.Q_JOD];
    end
    all_names = [all_names;convertCharsToStrings(dis_upscaled_name)]
    all_scores = [all_scores; mean(dis_score)];
    all_scores_p_det = [all_scores_p_det; mean(p_det_score)];
    all_scores_c_max = [all_scores_c_max; mean(c_max_score)];
    all_scores_q_jod = [all_scores_q_jod; mean(q_jod_score)];
end
scores = table(all_names,all_scores,all_scores_p_det,all_scores_c_max,all_scores_q_jod)
writetable(scores,'vdp3-dry-run-v3.csv')
disp("Finished all");