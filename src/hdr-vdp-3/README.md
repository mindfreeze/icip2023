# HDR-VDP3 Measurement Guide

## Description
1. The video list is stored as `src_vid_dist.csv`
2. The matlab runs through the list
3. Assign Width, Height, and Viewing distance in hdrvdp_pix_per_deg as parameters.
4. Note that, HDR-VDP-3 works as a per-frame metric, so `yuv_import` function seeks
the YUV file based on the frame-number.
5. The final score is computed as mean for different metrics.


## Steps to run

1. [Clone/Download](https://sourceforge.net/projects/hdrvdp/files/) HDR-VDP-3 to `hdr-vdp-3` folder,
2. Run the `hdrvdp3-subjective.m`



*Note*: The source-code for this is adapted from
[hdr-fr-code](https://github.com/JoshuaEbenezer/hdr_fr_code/)
